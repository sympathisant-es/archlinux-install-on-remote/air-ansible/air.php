Ansible Archlinux Php
=====================

A role to install php or php-legacy and set basic config on Archlinux

It may also install composer.

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)


Todo
----
- Be Kind
